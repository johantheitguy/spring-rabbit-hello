package com.example.demo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.SendTo;

@SpringBootApplication
@EnableBinding(HelloChannels.class)
public class SpringRabbitHelloBackendApplication {

	private static final Log log = LogFactory.getLog(SpringRabbitHelloBackendApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(SpringRabbitHelloBackendApplication.class, args);
	}
	
	@Bean
	public Jackson2JsonMessageConverter converter() {
		return new Jackson2JsonMessageConverter();
	}
	
	@StreamListener(HelloChannels.REQUEST)
	@SendTo(HelloChannels.RESPONSE)
	public Message<HelloResponse> hello(Message<HelloRequest> request) throws InterruptedException {
//		log.info("Received hello request: " + request.toString());
		Thread.sleep(200);
		return MessageBuilder.withPayload(new HelloResponse("Hello " + request.getPayload().name))
				.copyHeaders(request.getHeaders())
				.build();
	}
	
	@RabbitListener(concurrency = "100", bindings = @QueueBinding(
			value = @Queue(value = "hellorequestrabbit", durable = "false"),
			exchange = @Exchange(value = "hellorequestrabbit"),
			key = "hellorequestrabbit"), group="hellorequestrabbit")
	public HelloResponse helloRabbit(HelloRequest request) throws InterruptedException {
//		log.info("Received rabbit hello request: " + request.toString());
		Thread.sleep(200);
		return new HelloResponse("Hello " + request.getName());
	}
}
