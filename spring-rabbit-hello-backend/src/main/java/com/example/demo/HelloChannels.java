package com.example.demo;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface HelloChannels {

	String REQUEST = "hellorequest";
	@Input(REQUEST)
	SubscribableChannel helloRequestChannel();

	String RESPONSE = "helloresponse";
	@Output(RESPONSE)
	MessageChannel helloResponseChannel();
	
}
