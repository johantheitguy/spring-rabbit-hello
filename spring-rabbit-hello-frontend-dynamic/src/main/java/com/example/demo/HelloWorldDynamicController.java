package com.example.demo;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldDynamicController {

	@Autowired
	private AmqpTemplate amqp;
	
	@GetMapping(path = "/hello/{target}/{name}")
	public String handleRequest(@PathVariable String target, @PathVariable String name) {
		Message response = amqp.sendAndReceive(target, target, MessageBuilder.withBody(("{ \"name\": \"" + name + "\"}").getBytes())
				.setContentType("application/json").build());
		return new String(response.getBody());
	}
		
}

