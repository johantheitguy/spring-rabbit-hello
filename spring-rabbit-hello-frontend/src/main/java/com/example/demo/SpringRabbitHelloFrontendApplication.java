package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.dsl.HeaderEnricherSpec;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;

@SpringBootApplication
@EnableBinding({ HelloChannels.class })
@IntegrationComponentScan
public class SpringRabbitHelloFrontendApplication {

	@Bean
	public IntegrationFlow headerEnricherFlow() {
		return IntegrationFlows.from(HelloGateway.GATEWAY)
				.enrichHeaders(HeaderEnricherSpec::headerChannelsToString)
				.channel(HelloChannels.REQUEST)
				.get();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(SpringRabbitHelloFrontendApplication.class, args);
	}
}
