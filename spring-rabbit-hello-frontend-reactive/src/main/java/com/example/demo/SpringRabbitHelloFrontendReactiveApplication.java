package com.example.demo;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.AsyncRabbitTemplate;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableRabbit
public class SpringRabbitHelloFrontendReactiveApplication {

	@Autowired ConnectionFactory connectionFactory;
	@Autowired RabbitTemplate rabbitTemplate;
	@Autowired AmqpAdmin admin;
	
	@Bean
	Queue replyToQueue() {
		Queue queue = QueueBuilder.nonDurable().autoDelete().exclusive().build();
		admin.declareQueue(queue);
		return queue;
	}
	
	@Bean
	AsyncRabbitTemplate template() {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
		container.addQueues(replyToQueue());
		return new AsyncRabbitTemplate(rabbitTemplate, container);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(SpringRabbitHelloFrontendReactiveApplication.class, args);
	}
}
