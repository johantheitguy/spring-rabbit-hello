package com.example.demo;

import java.util.concurrent.Future;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.GatewayHeader;
import org.springframework.integration.annotation.MessagingGateway;

@MessagingGateway(defaultRequestChannel = "hellogateway", defaultReplyChannel = HelloChannels.RESPONSE, defaultHeaders = { @GatewayHeader(name="contentType", value="application/json") })
public interface HelloGateway {
	
	public static final String GATEWAY = "hellogateway";

	@Gateway
	public Future<String> hello(String request);
	
}
