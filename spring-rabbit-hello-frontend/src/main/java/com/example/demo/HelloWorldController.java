package com.example.demo;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
	
	@Autowired HelloGateway gateway;
	
	@GetMapping("/hello")
	public String hello() throws TimeoutException, ExecutionException, InterruptedException {

		Future<String> future1 = gateway.hello("{ \"name\": \"Joe\"}");
		Future<String> future2 = gateway.hello("{ \"name\": \"Soap\"}");

		return 
			future1.get(2000, TimeUnit.MILLISECONDS) + " and " + 
			future2.get(2000, TimeUnit.MILLISECONDS);
	}

}
