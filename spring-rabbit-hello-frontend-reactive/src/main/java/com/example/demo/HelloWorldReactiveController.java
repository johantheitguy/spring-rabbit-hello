package com.example.demo;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.AsyncRabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
public class HelloWorldReactiveController {
	
	@Autowired AsyncRabbitTemplate rabbit;

	@GetMapping(path = "/hello/{target}/{name}")
	public Mono<String> helloAsync(@PathVariable String target, @PathVariable String name) {
		ListenableFuture<Message> response = 
				rabbit.sendAndReceive(target, target, MessageBuilder.withBody(("{ \"name\": \"" + name + "\"}").getBytes())
						.setContentType("application/json").build());
		return Mono.fromFuture(response.completable()).map(o -> new String(o.getBody()));
	}
		
}

